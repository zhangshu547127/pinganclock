package com.paic.arch.interviews.TimeConverterImpl;

import com.paic.arch.interviews.TimeConverter.TimeConverter;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

/**
 * Created by zhangshu on 2018/3/6.
 */
public class TimeConverterImpl implements TimeConverter{

 /**
  * 由测试用例 可知该需求是要求根据当前日期生成一个字符串
  * Y 黄灯 R红灯 0灭灯
  *   When the time is 00:00:00
      Y
      RRRR
      RRRR
      OOOOOOOOOOO
      OOOO
      ------------
      When the time is 00:00:00
      Y
      OOOO
      OOOO
      OOOOOOOOOOO
      OOOO
      When the time is 13:17:01
      O
      RROO
      RRRO
      YYROOOOOOOO
      YYOO
  */
    private String LIGHT_YELLOW = "Y";

    private String LIGHT_RED = "R";

    private String LIGHT_DEFAULT = "O";
    @Override
    public String convertTime(String aTime) {
        //定义结果
        StringBuffer result = new StringBuffer();
        //首先校验格式格式不符直接返回
        String regex = "\\d{2}:\\d{2}:\\d{2}";
        if(!StringUtils.isBlank(aTime) && aTime.matches(regex)){
            String[] strs = aTime.split(":");
            int hour = evalInt(strs[0]);
            int min = evalInt(strs[1]);
            int second = evalInt(strs[2]);
            result.append(getFirstLine(second));
            result.append(getSecondLine(hour));
            result.append(getThirdLine(hour));
            result.append(getFourthLine(min));
            result.append(getFifthLine(min));
        }
        //System.out.println(result.toString());
        return result.toString();
    }
    //第5行 每分钟多亮一个黄灯;
    private String getFifthLine(int min) {
        String result = "";
        int lightNum = min%5;
        result = StringUtils.rightPad(result,lightNum,LIGHT_YELLOW);
        return StringUtils.rightPad(result.toString(),4,LIGHT_DEFAULT);
    }

    //第四行 每5分钟多亮一个灯,其中15分钟 30分钟 45分钟为红色 其余为黄色;
    private String getFourthLine(int min) {
        StringBuffer result = new StringBuffer();
        int lightNum = min/5;
        for(int i=1;i<=11;i++){
            if(lightNum>=i){
                if(i%3==0){
                    result.append(LIGHT_RED);
                }else{
                    result.append(LIGHT_YELLOW);
                }
            }else {
                result.append(LIGHT_DEFAULT);
            }
        }
        return result.append("\r\n").toString();
    }
    //第三行 每小时多亮一个红灯,5小时清空一次
    private String getThirdLine(int hour) {
        String result = "";
        int lightNum = hour%5;
        result = StringUtils.rightPad(result,lightNum,LIGHT_RED);
        return StringUtils.rightPad(result.toString(),4,LIGHT_DEFAULT)+"\r\n";
    }

    //第二行 每5小时多亮一个红灯
    private String getSecondLine(int hour) {
        String result = "";
        int lightNum = hour/5;
        result = StringUtils.rightPad(result,lightNum,LIGHT_RED);
        return StringUtils.rightPad(result.toString(),4,LIGHT_DEFAULT)+"\r\n";
    }

    //第一行 偶数秒黄灯 奇数秒0
    private String getFirstLine(int second) {
        return (second%2 == 0?LIGHT_YELLOW:LIGHT_DEFAULT)+"\r\n";
    }

    public static int evalInt(Object obj) {
        if (obj == null)
            return -1000;
        return NumberUtils.toInt(obj.toString(), -1000);
    }
}
