package com.paic.arch.interviews.TimeConverter;

public interface TimeConverter {

    String convertTime(String aTime);

}
